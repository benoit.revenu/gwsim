import pandas as pd 
import glob 
import json
import tqdm


class Catalog_MiceCat(object):
    #"/mnt/zfshome4/cbc.cosmology/MDC/Catalogs/MiceCatv1/*parq"
    def __init__(self, path, magnitude_band):
        self.path_file = path
        self.magnitude_band = magnitude_band
    
    self.flist = glob.glob(self.path_file)
    catalog = pd.DataFrame()
    
    for fname in tqdm(flist):
        catalog_ = pandas.read_parquet(fname)
        catalog_ = catalog_[['z_cgal','ra_gal','dec_gal',self.magnitude_band]]
        catalog = pd.concat(catalog_, catalog)
        
    catalog.rename(columns = {'z_cgal': 'z','ra_gal': 'ra','dec_gal':'dec', 'magnitude': self.magnitude_band}, inplace = True)
    self.catalog = catalog
    

class Catalog_GLADEPlus(object):
    #'/home/cbc.cosmology/MDC/Catalogs/GLADE+/GLADE+.txt'
    def __init__(self, path, magnitude_band):
        self.path_file = path
        self.magnitude_band = magnitude_band
    
    chunksize = int(10**6)
    df = pd.read_csv(fn, header=None, delim_whitespace=True, 
                     usecols=[8,9,10,18, 28], chunksize = chunksize)
    catalog = pd.DataFrame()
    for chunk in tqdm(df, desc = 'Loading GLADE+ catalog'):
        catalog = pd.concat([catalog, chunk])
    catalog.columns = ['RA', 'dec', 'mB','mK', 'z']
    catalog = catalog[['z', 'RA', 'dec', 'm'+opts.Lband]]
    catalog = catalog[(catalog['z']>0.0)]
    absM = cosmo.M_mdL(catalog[['m'+opts.Lband]], catalog.z)
    catalog[opts.Lband] = absM