import pandas as pd
from tqdm import tqdm 
import numpy as np
import matplotlib.pyplot as plt
from optparse import Option, OptionParser, OptionGroup
import fitsio
import os, sys
import pandas as pd
from luminosity_function import LuminosityFunction
import pickle 
import shutil

parser = OptionParser(
    description = __doc__,
    usage = "%prog [options]",
    option_list = [
        Option("--name_file",default=None,type=str,help="Name of folder to save + BAND"), 
        Option("--catalog_name",default=None,type=str,help="Catalog name, MiceCatV2 or GLADE+"), 
        Option("--H0", default=70,type=float, help="COSMOLOGY: H0 value"), 
        Option("--Omega_m", default = 0.3,type=float, help="COSMOLOGY: Omega_m value"),
        Option("--w0", default = -1,type=float,help="COSMOLOGY: EoS value"),
        Option("--cosmo_model",type=str, default = 'FlatLambdaCDM',help="COSMOLOGY: Cosmology Value"),
        Option("--Lmin", default = -15,type=float,help="LUMINOSITY: Minimum luminosity"),
        Option("--Lmax", default = -23,type=float,help="LUMINOSITY: Maximum luminosity"),
        Option("--alpha_start",  default = -1.1,type=float,help="LUMINOSITY: Alpha value start"),
        Option("--Mstar_start", default = -20.55,type=float,help="LUMINOSITY: Mstar value start"),
        Option("--phistar_start", default = 0.005,type=float,help="LUMINOSITY: phi_star value start"),
        Option("--fraction", default = 1,type=float,help="GALAXIES: fraction of the sky covered"),
        Option("--zmax", default = 2,type=float,help="GALAXIES: zmax"),
        Option("--zmin", default = 0,type=float,help="GALAXIES: zmin"),
        Option("--Nbins", default = 20,type=int,help="LUMINOSITY: number of luminoisty bins to evaluate luminosity function"),
        Option("--Nbins_z", default = 20,type=int,help="GALAXIES: number of redshift bins to evaluate luminosity function"),
        Option("--Lband", default = None,type=str,help="LUMINOSITY: Luminosity band")])


opts, args = parser.parse_args()

#Save flow in folder 
folder_name = str(opts.name_file) + '_' + str(opts.Lband)
# path = 'trained_flows_and_curves/'
path = 'catalogs_fitting_data/{}/'.format(opts.catalog_name)

#check if directory exists
if os.path.exists(path+folder_name):
    #if yes, delete directory
    shutil.rmtree(path+folder_name)

#Save model in folder
os.mkdir(path+folder_name)





cosmo_para = {'H0':opts.H0, 'Omega_m': opts.Omega_m, 'w0': opts.w0, 'cosmo_model':opts.cosmo_model}
sys.path.append('..')
import cosmology
cosmo = cosmology.Cosmology(cosmo_para, zmax = 10)
fitting_para = {'Lmin': opts.Lmin,'Lmax':opts.Lmax, 'band':opts.Lband, 'alpha_start': opts.alpha_start,
                'Mstar_start':opts.Mstar_start, 'phistar_start': opts.phistar_start, 'fraction':opts.fraction}



## Load Catalog
if opts.catalog_name == 'MiceCatV2':
    chunksize = int(10**6)
    df = pd.read_csv('/home/federico.stachurski/LVK/load_catalog/small_cat_10.csv', chunksize=chunksize)
    catalog = pd.DataFrame()
    for chunk in tqdm(df, desc = 'Loading MiceCatv2 catalog'):
        catalog = pd.concat([catalog, chunk])

    catalog = catalog[['z_cgal','ra_gal','dec_gal',opts.Lband]]
    catalog = catalog.dropna()
    catalog = catalog[catalog['z_cgal']>0]
    catalog.rename(columns = {'z_cgal': 'z','ra_gal': 'ra','dec_gal':'dec'}, inplace = True)
                      
                      
elif opts.catalog_name == 'GLADE+':
    fn = '/home/cbc.cosmology/MDC/Catalogs/GLADE+/GLADE+.txt'
    chunksize = int(10**6)
    df = pd.read_csv(fn, header=None, delim_whitespace=True, 
                     usecols=[8,9,10,18, 28], chunksize = chunksize)
    catalog = pd.DataFrame()
    for chunk in tqdm(df, desc = 'Loading GLADE+ catalog'):
        catalog = pd.concat([catalog, chunk])
    catalog.columns = ['RA', 'dec', 'mB','mK', 'z']
    catalog = catalog[['z', 'RA', 'dec', 'm'+opts.Lband]]
    catalog = catalog[(catalog['z']>0.0)]
    absM = cosmo.M_mdL(np.array(catalog[['m'+opts.Lband]]), np.array(catalog.z))
    catalog[opts.Lband] = absM



    
### Instantiate Luminosity class
LF_class = LuminosityFunction(catalog, fitting_para, cosmo)    

### Initiate lists to save parameters 
alpha, alpha_err, Mstar, Mstar_err, phistar, phistar_err, completeness, completeness_err, zbin_val= [], [], [], [], [], [], [], [], []

#Make zbins 
zbins = np.linspace(opts.zmin, opts.zmax, int(opts.Nbins_z)+1)
for i in range(opts.Nbins_z):
    if i == opts.Nbins_z - 1:
        break
    sys.stdout.write('\rComputing completeness between [{},{}]'.format(zbins[i],zbins[i+1]))
    result = LF_class.fit_LF( zmax = zbins[i+1], zmin = zbins[i], Nbins = int(opts.Nbins))
    parameters_result = LF_class.plot_fig_completeness(result, 'catalogs_fitting_data/MiceCatV2/{}'.format(folder_name))
    
    alpha.append(parameters_result[0])
    alpha_err.append(parameters_result[1])
    Mstar.append(parameters_result[2])
    Mstar_err.append(parameters_result[3])
    phistar.append(parameters_result[4])
    phistar_err.append(parameters_result[5])
    completeness.append(parameters_result[6])
    completeness_err.append(parameters_result[7])
    zbin_val.append(zbins[i+1])
    

para_dict = {'alpha': alpha, 'alpha_err': alpha_err, 'Mstar': Mstar, 'Mstar_err': Mstar_err, 
             'phistar': phistar, 'phistar_err':phistar_err, 
             'completeness': completeness, 'completeness_err': completeness_err, 'zbin_val':zbin_val}
para_dict.update(fitting_para)
para_dict.update(cosmo_para)

with open('catalogs_fitting_data/{}/{}/fitting_parameters_completeness.pkl'.format(opts.catalog_name, folder_name), 'wb') as fp:
    pickle.dump(para_dict, fp)