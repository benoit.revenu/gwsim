import pandas as pd
from tqdm import tqdm 
import numpy as np
import matplotlib.pyplot as plt
import lmfit 


class LuminosityFunction(object):
    def __init__(self, pre_proc_catalog, fitting_parameters, cosmology):
        self.catalog = pre_proc_catalog #z, ra, dec
        self.fitting_parameters = fitting_parameters #LMin, magnitude band, .alpha_start, Mstar_starts, phistar_start
        self.cosmology = cosmology
        
    def fit_LF(self, zmax, zmin, Nbins = 20):
        #Cut Catalog in redshift bin
        cut_cat = self.catalog[self.catalog[self.fitting_parameters['band']]<self.fitting_parameters['Lmin']]
        #get the Magnitude distribution for the redshift bin
        cut_cat = cut_cat[(cut_cat['z']<zmax) & (cut_cat['z']>zmin)] 
        boundaries = np.min(cut_cat[self.fitting_parameters['band']]) , np.max(cut_cat[self.fitting_parameters['band']])
        y_vals, x_vals = np.histogram(cut_cat[self.fitting_parameters['band']], bins = Nbins, density = 0, range = (boundaries[0], boundaries[1]))
        
        #Clean values
        dx = np.diff(x_vals)[0]
        x_vals = x_vals[:-1] #
        y_err = np.sqrt(y_vals)
        inx_0 = np.where(y_vals==0)[0]
        y_vals[inx_0] = 1
        y_err[inx_0] = 1

        # check where maximum point of LF is and perform fit from there 
        inx_max_y = np.where(y_vals == np.max(y_vals))[0] 
        highM = x_vals[inx_max_y][0]
        M_array = x_vals
        y_high = y_vals[M_array < highM]
        y_high_err = y_err[M_array < highM]
        M_array_high = x_vals[M_array < highM]
        

        # initialise fitting parameters
        Mstar_start = self.fitting_parameters['Mstar_start']#-20.55
        alpha_start = self.fitting_parameters['alpha_start']#-1.05
        phi_star = self.fitting_parameters['phistar_start']#0.005

        fit_params = lmfit.Parameters()
        fit_params.add('alpha', value=alpha_start, min=-2, max=-0.1)
        fit_params.add('Mstar', value=Mstar_start, min=-25, max=-10)
        fit_params.add('phi_star', value=phi_star, min=0.0001, max=10)

        # perfomr least sqare fit using lmfit
        def residual(params, x, data):
            alpha = params['alpha'].value
            Mstar = params['Mstar'].value
            phi_star = params['phi_star'].value
            N_fit = self.cosmology.N_count_M_array(x, dx, zmin, zmax, alpha, Mstar, phi_star, fraction = self.fitting_parameters['fraction'])
            return abs(data - N_fit)**(2)/(y_high_err)**(2)

        result = lmfit.minimize(residual, fit_params, args=(M_array_high, y_high), method='least_squares')
        return {'result':result, 'M_array_high':M_array_high, 'M_array':M_array, 'y_vals':y_vals, 'y_err':y_err, 'y_high':y_high, 'y_high_err':y_high_err,
                'Nbins':Nbins, 'highM':highM, 'zmin':zmin , 'zmax':zmax}
    
    
    
    def plot_fig_completeness(self, result_fit, path_save): #, M_array_high, M_array, y_vals, y_err,yhigh, y_high_err, x_vals, zmin , zmax):
        #Read In results from fit
        dM = abs(np.diff(result_fit['M_array'])[0])
        zmax = result_fit['zmax']
        zmin = result_fit['zmin']
        x_vals = result_fit['M_array']
        fitted_parameters = (result_fit['result']).params
        alpha_fit = round(fitted_parameters['alpha'].value, 10) 
        alpha_fit_err = round(fitted_parameters['alpha'].stderr, 10)
        Mstar_fit = round(fitted_parameters['Mstar'].value,10)  
        Mstar_fit_err = round(fitted_parameters['Mstar'].stderr,10)
        phi_star_fit = abs(round(fitted_parameters['phi_star'].value, 10))
        err_phi_fit = round(abs(fitted_parameters['phi_star'].stderr), 10)
        M_array_total = np.linspace(self.fitting_parameters['Lmax'],self.fitting_parameters['Lmin'], 100)
        dM_total = np.diff(M_array_total)[0]
        
        n_err_samples = 1000
        
        samples_alpha = np.random.normal(alpha_fit, alpha_fit_err, size = n_err_samples)
        samples_Mstar = np.random.normal(Mstar_fit, Mstar_fit_err, size = n_err_samples)
        samples_phi = np.random.normal(phi_star_fit, err_phi_fit, size = n_err_samples)
        
        # print(samples_alpha, samples_Mstar, samples_phi)
        
        
        #Compute Ntotal for fit
        N = self.cosmology.N_count_M_array(M_array_total,dM, zmin, zmax, alpha_fit, Mstar_fit, phi_star_fit,  fraction = self.fitting_parameters['fraction'])
        N_gal_theory = np.trapz(N, x = M_array_total) #Compute completeness
        N_gal_data = np.trapz(result_fit['y_vals'], x = x_vals)
        completeness = N_gal_data / N_gal_theory #######COMPLETENESS (Density)
        N_high = self.cosmology.N_count_M_array(result_fit['M_array_high'],dM, zmin, zmax, alpha_fit, Mstar_fit, phi_star_fit,  fraction = self.fitting_parameters['fraction'])
        
        completeness_samples = []
        for i in tqdm(range(n_err_samples), desc = 'Computing Completenss for {} samples in range [{},{}] '.format(n_err_samples, round(zmin,3) , round(zmax,3))):
            N_temp = self.cosmology.N_count_M_array(M_array_total,dM, zmin, zmax, samples_alpha[i], samples_Mstar[i], samples_phi[i],  fraction = self.fitting_parameters['fraction'])
            N_temp = np.trapz(N_temp, x = M_array_total) #Compute completeness for different samples 
            completeness_samples.append(N_gal_data / N_temp)
        
        std_completeness = np.std(np.array(completeness_samples))
        

        
        ####### PLOTTING ###########
        fig, ax = plt.subplots(2,1, figsize = (8,8), gridspec_kw={'height_ratios': [3,1]})
        props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
        textstr = '\n'.join((
            r'$\alpha = {} \pm {}$'.format(round(alpha_fit, 4) , round(alpha_fit_err, 4) ),
            r'$M^* = {} \pm {}$'.format(round(Mstar_fit,4) , round(Mstar_fit_err,4 )),
            r'$\phi^* = {} \pm {}$'.format(phi_star_fit , err_phi_fit)))

        textstr_z = r'$z = {} - {}$'.format(round(zmin,3) , round(zmax,3) )
        line1, = ax[0].plot(M_array_total, N,'--b', lw=3, label = 'total')
        
        line1, = ax[0].plot(result_fit['M_array_high'], N_high,'--r', lw=3, label = 'fit')
        ax[0].fill_between(M_array_total, N,color='blue', alpha = 0.1, label = r'$P_{out}$')

        ax[0].errorbar(x_vals, result_fit['y_vals'], yerr=result_fit['y_err'], fmt='o', capsize=5, markersize = 7, elinewidth = 3, capthick = 3, 
                       label = 'data; {}-{}'.format(round(zmin,3) , round(zmax,3)), color = 'black', alpha = 1)
        ax[0].fill_between(x_vals, result_fit['y_vals'],color='red', alpha = 0.3, label = r'$P_{in}$')
        
        # title = ax[0].set_title(r'$Nbins = {} ; L_f = {}$'.format(Nbins, highM), fontsize = 20)
        # ax[0].text(0.05, 0.30, textstr, transform=ax[0].transAxes, fontsize=8,verticalalignment='top', bbox=props)

        ax[0].set_ylabel('$N_{gal}$', fontsize = 23)
        ax[0].set_yscale('log')
        ax[0].set_xlim([self.fitting_parameters['Lmin'], self.fitting_parameters['Lmax']])
        ax[0].set_ylim([1, 10*np.max(result_fit['y_vals'])])
        ax[0].grid(True)
        ax[0].legend(loc = 'lower left', fontsize = 15)
        ax[0].xaxis.set_tick_params(labelsize=12)
        ax[0].yaxis.set_tick_params(labelsize=12)
        fig.tight_layout() 

        yhigh = result_fit['y_high']
        residuals = 100*abs((yhigh - N_high)/yhigh)

        ax[1].plot(result_fit['M_array_high'], abs(residuals),'-ob', label = 'relative residuals')
        ax[1].set_xlim([self.fitting_parameters['Lmin'],self.fitting_parameters['Lmax']])
        ax[1].set_ylim([0.01,1e4])
        ax[1].axhline(y = 0 , linewidth = 3 , color = 'r', linestyle = 'dashed')
        ax[1].set_yscale('log')
        ax[1].set_xlabel('$M_{r} - 5log(h)$', fontsize = 20)
        ax[1].set_ylabel('$|\%\Delta_{{y}}/y|$', fontsize = 20)
        ax[1].grid(True)
        ax[1].legend(loc = 'upper left', fontsize = 15)
        ax[1].xaxis.set_tick_params(labelsize=12)
        fig.tight_layout() 

        plt.savefig(path_save+'/{}_{}_z_fit.png'.format(round(zmin,3) , round(zmax,3)), facecolor='white')
        plt.show()
        return  alpha_fit, alpha_fit_err, Mstar_fit, Mstar_fit_err, phi_star_fit, err_phi_fit, completeness, std_completeness