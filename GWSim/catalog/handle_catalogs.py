import pandas as pd 
import glob 
import json
from tqdm import tqdm


class Catalog_MiceCat(object):
    #"/mnt/zfshome4/cbc.cosmology/MDC/Catalogs/MiceCatv2/*parq"
    def __init__(self, path, magnitude_band):
        self.path_file = path
        self.magnitude_band = magnitude_band
        self.flist = glob.glob(self.path_file + '/*parq')
        catalog = pd.DataFrame()
        for fname in tqdm(self.flist, desc = 'Loading MiceCat'+self.path_file[-1]+' catalog'):
            catalog_ = pd.read_parquet(fname)
            if self.path_file[-1] == '2': #If MiceCatv2
                catalog_ = catalog_[['z_cgal','ra_gal','dec_gal',self.magnitude_band]]
            elif self.path_file[-1] == '1': #If MiceCatv1
                catalog_ = catalog_[['z','ra','dec',self.magnitude_band]]
            else: 
                raise ValueError('MiceCat 3 or above does not exist!')
                
            catalog = pd.concat([catalog_, catalog])
                        
        if self.path_file[-1] == 2:
            catalog.rename(columns = {'z_cgal': 'z','ra_gal': 'ra','dec_gal':'dec',
                                      'magnitude': self.magnitude_band}, inplace = True)
        elif self.path_file[-1] == 1:
            catalog.rename(columns = {'z': 'z','ra': 'ra','dec':'dec',
                                      'magnitude': self.magnitude_band}, inplace = True)
            
  
        self.catalog = catalog
        
        

class Catalog_GLADEPlus(object):
    #'/home/cbc.cosmology/MDC/Catalogs/GLADE+/GLADE+.txt'
    def __init__(self, path, magnitude_band, chuncksize = int(10**6)):
        ### CATALOG IN DEGREES ###
        self.path_file = path
        self.magnitude_band = magnitude_band
        self.fn = self.path_file+'/GLADE+.txt'
        self.chunksize = chuncksize
        
        if self.magnitude_band != 'K' and self.magnitude_band != 'B':
            raise ValueError('magnitude band does not exists in the GLADE+ file')
            
        df = pd.read_csv(self.fn, header=None, delim_whitespace=True, 
                         usecols=[8,9,10,18, 28, 31], chunksize = self.chunksize)
        catalog = pd.DataFrame()
        for chunk in tqdm(df, desc = 'Loading GLADE+ catalog'):
            catalog = pd.concat([catalog, chunk])
        catalog.columns = ['RA', 'dec', 'mB','mK', 'z', 'sigmaz']
        catalog = catalog[['z', 'RA', 'dec', 'm'+self.magnitude_band, 'sigmaz']]
        catalog = catalog.dropna()
        self.catalog = catalog[(catalog['z']>0.0)]
        